/*
 * Roles
 */

export const USER_ROLE_USER = 'User';
export const USER_ROLE_TEAM_ADMIN = 'TeamAdmin';
export const USER_ROLE_SYSTEM_ADMIN = 'SystemAdmin';

/**
 * Routing
 */

export const HOME_URL = '/';
export const TEAMS_URL = '/teams';
export const ADMIN_URL = '/admin';
export const ADMIN_SUB1_URL = '/sub1';
export const ADMIN_SUB2_URL = '/sub2';
export const ADMIN_SUB3_URL = '/sub3';
export const ADMIN_SUB4_URL = '/sub4';
export const SEARCH_URL = '/search';
export const HELP_URL = '/help';

export const TOP_LEVEL_ROUTES = [
  {
    exact: true,
    name: 'Home',
    path: HOME_URL,
    allowedRoles: [USER_ROLE_USER, USER_ROLE_TEAM_ADMIN, USER_ROLE_SYSTEM_ADMIN]
  },
  {
    name: 'Teams',
    path: TEAMS_URL,
    allowedRoles: [USER_ROLE_TEAM_ADMIN, USER_ROLE_SYSTEM_ADMIN]
  },
  {
    name: 'Admin',
    path: ADMIN_URL,
    allowedRoles: [USER_ROLE_SYSTEM_ADMIN]
  },
  {
    name: 'Search',
    path: SEARCH_URL
  },
  {
    name: 'Help',
    path: HELP_URL
  }
];

/*
 * Colors
 */

export const COLORS = {
  PRIMARY: '#2e63b2',
  ERROR: '#f46969',
  GREY_DARK: '#333333',
  GREY_DARKEN: '#414141',
  GREY: '#9a9a9a',
  GREY_LIGHTEN: '#c9c9c9',
  GREY_LIGHT: '#e4e4e4',
  GREY_LIGHTER: '#f6f6f6',
  GREY_BORDER: '#d8d8d8',
  LIGHT: '#fff',
  LIGHT_DISABLED: 'rgba(255, 255, 255, 0.56)'
};

export const LOGO_URL = '/img/logo.png';
export const DEFAULT_AVATAR_URL = '/img/default_avatar.png';

export const STORAGE_KEY = 'basicExample';

export const TABLE_FETCH_LIMIT = 50;

export const PROJECTS_LIST_HEADER_FIELDS = [
  {
    name: 'Project Name',
    field: 'name'
  },
  {
    name: 'Author',
    field: 'author'
  },
  {
    name: 'Start Date',
    field: 'startDate'
  },
  {
    name: 'Status',
    field: 'status'
  }
];

export const PROJECT_STATUSES = {
  'Completed': {
    color: 'green',
    priority: 2
  },
  'In progress': {
    color: 'blue',
    priority: 1
  },
  'Suspended': {
    color: 'red',
    priority: 0
  }
};
