/* eslint-disable */
import React from 'react';
import { shallow } from 'enzyme';

// jest.mock('helpers/navigate');

import CoreLayout from '../CoreLayout';
import CoreLayoutContainer from '../../CoreLayout';
import createMockStore from 'helpers/createMockStore';

describe('CoreLayoutContainer tests', () => {
  it('is rendered', () => {
    const shallowCoreLayoutContainer = shallow(
      <CoreLayoutContainer store={createMockStore({ router: {}, user: { isFetching: false } })} />
    );

    expect(shallowCoreLayoutContainer.exists()).toEqual(true);
  });
});

describe('CoreLayout tests', () => {
  let props;
  let shallowCoreLayout;
  const mockChildren = <div>Mock children</div>;
  const coreLayout = () => {
    if (!shallowCoreLayout)
      shallowCoreLayout = shallow(
        <CoreLayout {...props} />
      );

    return shallowCoreLayout;
  };

  beforeEach(() => {
    props = {
      isFetching: false,
      location: { pathname: '/' },
      children: mockChildren
    };
    shallowCoreLayout = undefined;
  });

  describe('When CoreLayout is mounted', () => {
    it('is rendered', () => {
      expect(coreLayout().exists()).toEqual(true);
    });

    describe('When User is fetching', () => {
      beforeEach(() => {
        props.isFetching = true;
      });

      it('renders Loader inside', () => {
        expect(coreLayout().find('Loader').length).toEqual(1);
      });
    });
  });
});
