import { connect } from 'react-redux';

import CoreLayout from './CoreLayout';

const mapStateToProps = state => ({
  location: state.router.location,
  isFetching: state.user.isFetching
});

export default connect(mapStateToProps)(CoreLayout);
