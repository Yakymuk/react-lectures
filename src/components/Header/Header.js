import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { withMediaQuery } from 'components/HOCs';
import MobileNavigation from 'components/MobileNavigation';
import HeaderUserMenu from './components/HeaderUserMenu';
import ProtectedNavLink from 'components/ProtectedNavLink';

import { LOGO_URL, HOME_URL, TOP_LEVEL_ROUTES } from 'consts';

import styles from './Header.scss';

class Header extends Component {
  static propTypes = {
    user                  : PropTypes.shape({
      userRoles             : PropTypes.arrayOf.isRequired
    }).isRequired,
    location              : PropTypes.shape({
      pathname              : PropTypes.string.isRequired
    }).isRequired,
    isMobileView          : PropTypes.bool.isRequired,
    setUserMenuState      : PropTypes.func.isRequired
  }

  render () {
    const { user, setUserMenuState, location, location: { pathname }, isMobileView } = this.props;
    const navigationList = TOP_LEVEL_ROUTES
      .map(({ exact, path, name, allowedRoles }) => (
        <ProtectedNavLink
          to={path}
          key={path}
          exact={exact}
          location={location}
          userRoles={user.userRoles}
          allowedRoles={allowedRoles}
          activeClassName={styles.beHeaderNavigationActiveLink}
        >
          {name}
        </ProtectedNavLink>
      ));

    return (
      <div className={styles.beHeader}>
        <div className={styles.beHeaderContent}>
          <Link
            to={HOME_URL}
            className={styles.beHeaderLogo}
          >
            <img
              src={LOGO_URL}
              alt='Page logo'
              className={styles.beHeaderLogoImg}
            />
          </Link>
          {isMobileView ? (
            <MobileNavigation pathname={pathname}>
              {navigationList}
            </MobileNavigation>
          ) : (
            <div className={styles.beHeaderNavigation}>
              {navigationList}
            </div>
          )}
          <div className={styles.beHeaderMenus}>
            <HeaderUserMenu
              user={user}
              setMenuState={setUserMenuState}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withMediaQuery('(max-width: 800px)', 'isMobileView')(Header);
