import React from 'react';
import PropTypes from 'prop-types';

import OutsideClick from 'components/OutsideClick';

import { DEFAULT_AVATAR_URL } from 'consts';

import styles from './HeaderUserMenu.scss';

const HeaderUserMenu = ({
  user: {
    role,
    name,
    photoUrl,
    isFetched,
    isFetching,
    isMenuVisible
  },
  setMenuState
}) => {
  const data = [
    { label: 'Name', value: name },
    { label: 'User Role', value: role }
  ];

  return (
    <OutsideClick
      isActive={isMenuVisible}
      onOutsideClick={() => setMenuState(false)}
    >
      <div className={`${styles.beHeaderUser} ${isMenuVisible ? styles.beHeaderUserActive : ''}`}>
        <div
          onClick={() => setMenuState(!isMenuVisible)}
          style={{ backgroundImage: `url(${photoUrl || DEFAULT_AVATAR_URL})` }}
          className={styles.beHeaderUserAvatar + (!photoUrl ? ` ${styles.beHeaderUserAvatarDefault}` : '')}
        />
        <div
          className={styles.beHeaderUserMenu}
          onClick={evt => evt.target.tagName.toLowerCase() === 'a' ? setMenuState(false) : null}
        >
          {isFetching && 'Loading...'}
          {data.map(({ label, value }, idx) => (
            <div
              key={idx}
              className={styles.beHeaderUserMenuItem}
            >
              <div className={styles.beHeaderUserMenuItemTitle}>{label}</div>
              <div className={styles.beHeaderUserMenuItemValue}>{value}</div>
            </div>
          ))}
          <span className={styles.beVersionLabel}>ver. 0.0.1</span>
        </div>
      </div>
    </OutsideClick>
  );
};

HeaderUserMenu.propTypes = {
  user                      : PropTypes.shape({
    isFetching              : PropTypes.bool.isRequired,
    isFetched               : PropTypes.bool.isRequired,
    isMenuVisible           : PropTypes.bool
  }).isRequired,
  setMenuState              : PropTypes.func.isRequired
};

export default HeaderUserMenu;
