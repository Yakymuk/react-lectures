import React from 'react';
import Loadable from './Loadable';

import Loader from 'components/Loader';
import { injectReducer } from 'store/makeRootReducer';

const RouteLoader = () => <Loader isBlocking isFullScreen />;

const LoadableRoute = (route, reducer, key) => {
  const onFirstLoad = () => {
    if (reducer && key)
      injectReducer({ key, reducer });
  };

  return Loadable({
    onFirstLoad,
    loader: route,
    loading: RouteLoader
  });
};

export default LoadableRoute;
