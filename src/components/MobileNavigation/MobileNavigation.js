import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import _ from 'lodash';

import OutsideClick from 'components/OutsideClick';
import { withToggle } from 'components/HOCs';

import styles from './MobileNavigation.scss';

const classNames = {
  enter: styles.beMobileNavigationContainerEnter,
  enterActive: styles.beMobileNavigationContainerEnterActive,
  exit: styles.beMobileNavigationContainerLeave,
  exitActive: styles.beMobileNavigationContainerLeaveActive
};

class MobileNavigation extends Component {
  static propTypes = {
    pathname                  : PropTypes.string.isRequired,
    children                  : PropTypes.any,
    closeNavigation           : PropTypes.func.isRequired,
    toggleNavigation          : PropTypes.func.isRequired,
    isNavigationActive        : PropTypes.bool.isRequired
  };

  render () {
    const {
      children,
      pathname,
      closeNavigation,
      toggleNavigation,
      isNavigationActive
    } = this.props;
    const activeChild = _.find(Children.toArray(children),
      ({ props: { exact, to } }) =>
        exact
          ? pathname === to
          : pathname.indexOf(to) === 0
    );

    return (
      <OutsideClick
        isActive={isNavigationActive}
        onOutsideClick={closeNavigation}
      >
        <div className={styles.beMobileNavigation}>
          <div
            onClick={toggleNavigation}
            className={
              styles.beMobileNavigationCurrent +
              (isNavigationActive ? ` ${styles.beMobileNavigationCurrentActive}` : '')
            }
          >
            {activeChild && activeChild.props.children}
          </div>
          <CSSTransition
            mountOnEnter
            unmountOnExit
            timeout={200}
            in={isNavigationActive}
            classNames={classNames}
          >
            <div className={styles.beMobileNavigationContainer}>
              <div className={styles.beMobileNavigationContainerInner}>
                <div className={styles.beMobileNavigationContent}>
                  {Children.map(children, child =>
                    cloneElement(child,
                      {
                        onClick: closeNavigation,
                        className: styles.beMobileNavigationLink,
                        activeClassName: styles.beMobileNavigationLinkActive
                      }
                    )
                  )}
                </div>
              </div>
            </div>
          </CSSTransition>
        </div>
      </OutsideClick>
    );
  }
}

export default _.flow(
  withToggle('Navigation'),
  withToggle('Navigation2'),
  withToggle('Navigation3')
)(MobileNavigation);
