import React, { Component } from 'react';

const withToggle = name => {
  const isActiveFieldName = `is${name}Active`;
  const closeFuncName = `close${name}`;
  const toggleFuncName = `toggle${name}`;

  return WrappedComponent => {
    class withToggleComponent extends Component {
      constructor (...args) {
        super(...args);

        this.state = {
          [isActiveFieldName]: false
        };

        this[closeFuncName] = this.close.bind(this);
        this[toggleFuncName] = this.toggle.bind(this);
      }

      toggle () {
        this.setState(prevState => ({
          [isActiveFieldName]: !prevState[isActiveFieldName]
        }));
      }

      close () {
        this.setState({
          [isActiveFieldName]: false
        });
      }

      render () {
        const dynamicProps = {
          [closeFuncName]: this[closeFuncName],
          [toggleFuncName]: this[toggleFuncName]
        };

        return (
          <WrappedComponent
            {...this.props}
            {...this.state}
            {...dynamicProps}
          />
        );
      }
    }
    return withToggleComponent;
  };
};

export default withToggle;
