import React from 'react';
import PropTypes from 'prop-types';

const PaginationArrow = ({ style, fillClassName, fillColor, isRight }) => (
  <svg
    width='8'
    height='14'
    style={style}
    aria-hidden='true'
    viewBox='0 0 8 14'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g
      fill='none'
      fillRule='nonzero'
    >
      <polygon
        fill={fillColor}
        className={`mdr-icon-fill ${fillClassName || ''}`}
        transform={isRight ? 'rotate(-180, 4, 6.5)' : ''}
        points='8.0 1.5 6.5 0.0 0.0 6.5 6.5 13.0 8.0 11.5 3.0 6.5'
      />
    </g>
  </svg>
);

PaginationArrow.defaultProps = {
  fillColor           : '#9A9A9A',
  isRight             : false
};

PaginationArrow.propTypes = {
  style               : PropTypes.object,
  isRight             : PropTypes.bool,
  fillColor           : PropTypes.string,
  fillClassName       : PropTypes.string
};

export default PaginationArrow;
