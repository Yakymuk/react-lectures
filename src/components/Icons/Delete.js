import React from 'react';
import PropTypes from 'prop-types';

const Delete = ({ style, fillClassName, fillColor }) => (
  <svg
    width='18'
    height='18'
    style={style}
    viewBox='0 0 18 18'
    xmlns='http://www.w3.org/2000/svg'
  >
    <g
      fill='none'
      fillRule='evenodd'
    >
      <path
        fill={fillColor}
        fillRule='nonzero'
        className={`mdr-icon-fill ${fillClassName || ''}`}
        d='M4.5 14.25c0 .825.675 1.5 1.5 1.5h6c.825 0 1.5-.675 1.5-1.5v-9h-9v9zM14.25 3h-2.625l-.75-.75h-3.75l-.75.75H3.75v1.5h10.5V3z' // eslint-disable-line max-len
      />
      <path d='M0 0h18v18H0z' />
    </g>
  </svg>
);

Delete.defaultProps = {
  fillColor           : '#9A9A9A'
};

Delete.propTypes = {
  style               : PropTypes.string,
  fillClassName       : PropTypes.string,
  fillColor           : PropTypes.string
};

export default Delete;
