import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import Switch from './ConnectedSwitch';
import { history } from 'store/createStore';
import CoreLayout from 'layouts/CoreLayout';
import NotFoundView from 'routes/NotFound';
import LoadableRoute from 'components/LoadableRoute';
import ProtectedRoute from 'components/ProtectedRoute';
import { getUserInfo } from 'store/reducers/user';
import helpReducer from 'routes/Help/modules/help';
import homeReducer from 'routes/Home/modules/home';
import searchReducer from 'routes/Search/modules/search';
import { TOP_LEVEL_ROUTES } from 'consts';

class App extends Component {
  static propTypes = {
    store  : PropTypes.object.isRequired
  }

  componentDidMount () {
    const { dispatch, getState } = this.props.store;
    const { user: { isFetched } } = getState();

    if (!isFetched) dispatch(getUserInfo());
  }

  render () {
    const { store } = this.props;
    const routeComponents = [
      LoadableRoute(() => import(/* webpackChunkName: 'home' */ 'routes/Home'), homeReducer, 'home'),
      LoadableRoute(() => import(/* webpackChunkName: 'teams' */ 'routes/Teams')),
      LoadableRoute(() => import(/* webpackChunkName: 'admin' */ 'routes/Admin')),
      LoadableRoute(() => import(/* webpackChunkName: 'search' */ 'routes/Search'), searchReducer, 'search'),
      LoadableRoute(() => import(/* webpackChunkName: 'help' */ 'routes/Help'), helpReducer, 'help')
    ];

    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <CoreLayout>
            <Switch>
              {TOP_LEVEL_ROUTES.map(({ exact, path, allowedRoles }, idx) => (
                <ProtectedRoute
                  key={path}
                  path={path}
                  exact={exact}
                  allowedRoles={allowedRoles}
                  component={routeComponents[idx]}
                />
              ))}
              <ProtectedRoute
                component={NotFoundView}
              />
            </Switch>
          </CoreLayout>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
