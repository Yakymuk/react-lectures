import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Table from 'components/Table';
import Loader from 'components/Loader';
import EmptyState from 'components/EmptyState';
import PaginationControl from 'components/PaginationControl';
import { TableIcon, Delete } from 'components/Icons';

import { PROJECTS_LIST_HEADER_FIELDS, PROJECT_STATUSES } from 'consts';

import styles from './ProjectsList.scss';


class ProjectsList extends Component {
  static propTypes = {
    items                   : PropTypes.array.isRequired,
    total                   : PropTypes.number.isRequired,
    caption                 : PropTypes.string.isRequired,
    options                 : PropTypes.shape({
      page                    : PropTypes.number,
      perPage                 : PropTypes.number,
      sortField               : PropTypes.string,
      isSortAscending         : PropTypes.bool
    }),
    isFetched               : PropTypes.bool.isRequired,
    isFetching              : PropTypes.bool.isRequired,
    isSortable              : PropTypes.bool,
    deleteProject           : PropTypes.func,
    handlePageChange        : PropTypes.func,
    handleSortChange        : PropTypes.func,
    isDeleteFetching        : PropTypes.bool
  }

  static defaultProps = {
    total                   : 0,
    options                 : {},
    isLoadMoreActive        : false,
    isDeleteFetching        : false
  }

  constructor (props, ...rest) {
    super(props, ...rest);

    this.renderStatus = this.renderStatus.bind(this);
    this.renderStartDate = this.renderStartDate.bind(this);

    this.actions = props.deleteProject
      ? [
        {
          icon: <Delete />,
          onAction: item => props.deleteProject(item.id)
        }
      ]
      : [];
  }

  renderStartDate ({ startDate }) {
    return startDate && moment(startDate).format('DD MMM YYYY');
  }

  renderStatus ({ status }) {
    const color = PROJECT_STATUSES[status] && PROJECT_STATUSES[status].color;

    return status &&
      <div
        style={{ color, borderColor: color }}
        className={styles.beProjectsListStatus}
      >
        {status}
      </div>;
  }


  render () {
    const {
      total,
      items,
      caption,
      isFetched,
      isFetching,
      isSortable,
      options: {
        page,
        perPage,
        sortField,
        isSortAscending
      },
      isDeleteFetching,
      handlePageChange,
      handleSortChange
    } = this.props;
    const pages = Math.ceil(total / perPage);

    return (
      <div className={styles.beProjectsList}>
        {(isFetching || isDeleteFetching) &&
          <Loader isBlocking />
        }
        <Table
          items={items}
          sortField={sortField}
          actions={this.actions}
          isSortable={isSortable}
          renderstatus={this.renderStatus}
          isSortAscending={isSortAscending}
          handleSortChange={handleSortChange}
          renderstartDate={this.renderStartDate}
          headerFields={PROJECTS_LIST_HEADER_FIELDS}
        >
          <div className={styles.beProjectsListHeader}>
            {caption}
          </div>
          {!items.length && isFetched &&
            <EmptyState
              infoIcon={TableIcon}
              caption='No data to display.'
              message='Please add more projects.'
            />
          }
        </Table>
        {pages > 1 &&
          <div className={styles.beProjectsListPagination}>
            <PaginationControl
              page={page}
              pages={pages}
              onChange={handlePageChange}
            />
          </div>
        }
      </div>
    );
  }
}

export default ProjectsList;
