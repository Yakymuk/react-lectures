import { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

class OutsideClick extends Component {
  static propTypes = {
    isActive           : PropTypes.bool.isRequired,
    children           : PropTypes.node.isRequired,
    onOutsideClick     : PropTypes.func.isRequired
  }

  static defaultProps = {
    isActive           : true
  }

  componentDidMount () {
    this.onDocumentClick = evt => this._onDocumentClick(evt);

    const { isActive } = this.props;

    if (isActive)
      this.addListener();
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.isActive !== this.props.isActive)
      if (nextProps.isActive) {
        this.addListener();
      } else {
        this.removeListener();
      }
  }

  componentWillUnmount () {
    this.removeListener();
  }

  addListener () {
    document.addEventListener('click', this.onDocumentClick);
    document.addEventListener('touchstart', this.onDocumentClick);
  }

  removeListener () {
    document.removeEventListener('click', this.onDocumentClick);
    document.removeEventListener('touchstart', this.onDocumentClick);
  }

  _onDocumentClick (evt) {
    let target = evt.target;
    let isInsideClick = false;

    const node = ReactDOM.findDOMNode(this);

    // node.contains is not work very well in IE
    while (target && target.parentNode && (target !== document)) {
      if (target === node) {
        isInsideClick = true;
        break;
      }
      target = target.parentNode;
    }

    if (!isInsideClick)
      this.props.onOutsideClick();
  }

  render () {
    return this.props.children;
  }
}

export default OutsideClick;
