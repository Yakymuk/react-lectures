import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from 'components/controls/Button';

import styles from './Clock.scss';

class Clock extends Component {
  static propTypes = {
    children    : PropTypes.func.isRequired
  };

  static defaultProps = {
    children    : () => {}
  };

  constructor (...args) {
    super(...args);

    this.state = {
      min: 0,
      sec: 0
    };

    this.handleStopClick = this.handleStopClick.bind(this);
    this.handleStartClick = this.handleStartClick.bind(this);
    this.handleResetClick = this.handleResetClick.bind(this);
  }

  componentWillUnmount () {
    this.handleStopClick();
  }

  handleStartClick () {
    this.timer = setInterval(() =>
      this.setState(prevState => ({
        sec: (prevState.sec + 1) % 60,
        min: prevState.sec === 59
          ? (prevState.min + 1) % 60
          : prevState.min
      })),
    1000);
  }

  handleStopClick () {
    clearInterval(this.timer);
  }

  handleResetClick () {
    this.setState({
      sec: 0,
      min: 0
    });
    this.handleStopClick();
    this.handleStartClick();
  }

  render () {
    const { children } = this.props;
    const { sec, min } = this.state;

    return (
      <div className={styles.beClock}>
        {children(min, sec)}
        <div className={styles.beClockControls}>
          <Button
            type='Primary'
            title='Start'
            name='be-clock-start-btn'
            onClick={this.handleStartClick}
          />
          <Button
            title='Stop'
            name='be-clock-stop-btn'
            onClick={this.handleStopClick}
          />
          <Button
            title='Reset'
            name='be-clock-reset-btn'
            onClick={this.handleResetClick}
          />
        </div>
      </div>
    );
  }
}

export default Clock;
