import React from 'react';
import PropTypes from 'prop-types';

import styles from './IconButton.scss';

const IconButton = ({
  style,
  onClick,
  children,
  disabled,
  className
}) => (
  <div
    style={style}
    onClick={disabled ? null : onClick}
    className={
      styles.beIconbutton +
      (className ? ` ${className}` : '') +
      (disabled ? ` ${styles.beIconbuttonDisabled}` : '')
    }
  >
    {children}
  </div>
);

IconButton.propTypes = {
  style         : PropTypes.object,
  onClick       : PropTypes.func.isRequired,
  children      : PropTypes.node.isRequired,
  disabled      : PropTypes.bool.isRequired,
  className     : PropTypes.string
};

IconButton.defaultProps = {
  disabled      : false
};

export default IconButton;
