import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './TextInput.scss';

class TextInput extends Component {
  static propTypes = {
    type            : PropTypes.string.isRequired,
    name            : PropTypes.string.isRequired,
    style           : PropTypes.object,
    value           : PropTypes.string.isRequired,
    width           : PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    height          : PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    onBlur          : PropTypes.func,
    onFocus         : PropTypes.func,
    inputRef        : PropTypes.func,
    disabled        : PropTypes.bool.isRequired,
    onChange        : PropTypes.func,
    onKeyDown       : PropTypes.func,
    errorText       : PropTypes.string.isRequired,
    autoFocus       : PropTypes.bool.isRequired,
    isCompact       : PropTypes.bool.isRequired,
    inputStyle      : PropTypes.object,
    placeholder     : PropTypes.string.isRequired,
    isMultiLine     : PropTypes.bool.isRequired,
    isActiveSelect  : PropTypes.bool.isRequired
  }

  static defaultProps = {
    type            : 'text',
    value           : '',
    width           : 280,
    height          : 40,
    onChange        : () => {},
    disabled        : false,
    errorText       : '',
    autoFocus       : false,
    isCompact       : false,
    placeholder     : 'Enter text',
    isMultiLine     : false,
    isActiveSelect  : false
  }

  componentDidUpdate (prevProps) {
    const { value, isMultiLine } = this.props;

    if (prevProps.value !== value && isMultiLine) {
      this.input.style.height = 'auto';
      this.input.style.height = `${this.input.scrollHeight + 2}px`;
    }
  }

  render () {
    const {
      type,
      name,
      style,
      value,
      width,
      height,
      onBlur,
      onFocus,
      inputRef,
      disabled,
      onChange,
      onKeyDown,
      errorText,
      isCompact,
      autoFocus,
      inputStyle,
      placeholder,
      isMultiLine,
      isActiveSelect
    } = this.props;
    const heightStyle = isMultiLine ? { minHeight: height } : { height };
    const commonProps = {
      value,
      onBlur,
      onFocus,
      disabled,
      onKeyDown,
      placeholder,
      id: `be-input-${name}`,
      style: {
        ...inputStyle,
        ...heightStyle,
        width
      },
      onChange: e => onChange(e.target.value),
      ref: input => {
        this.input = input;
        if (input && autoFocus) input.focus();
        if (inputRef) inputRef(input);
      },
      className: styles.beInput +
        (isCompact ? ` ${styles.beInputCompact}` : '') +
        (errorText ? ` ${styles.beInputInvalid}` : '') +
        (isActiveSelect ? ` ${styles.beInputActiveSelect}` : '')
    };

    return (
      <div
        style={{ ...style, width }}
        className={styles.beInputContainer}
      >
        {isMultiLine
          ? (
            <textarea
              {...commonProps}
              className={`${commonProps.className} ${styles.beInputTextarea}`}
            />
          ) : (
            <input
              type={type}
              {...commonProps}
            />
          )
        }
        {errorText &&
          <span className={styles.beInputError}>
            {errorText}
          </span>
        }
      </div>
    );
  }
}

export default TextInput;
