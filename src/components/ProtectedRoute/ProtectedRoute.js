import React from 'react';
import PropTypes from 'prop-types';

import { Route } from 'react-router-dom';

import NotFoundView from 'routes/NotFound';

const propTypes = {
  userRoles:        PropTypes.array.isRequired,
  component:        PropTypes.func.isRequired,
  allowedRoles:     PropTypes.array
};

const ProtectedRoute = ({ component: Component, userRoles, allowedRoles, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      !allowedRoles || userRoles.some(role => allowedRoles.indexOf(role) !== -1)
        ? <Component {...props} />
        : <NotFoundView />
    )}
  />
);

ProtectedRoute.propTypes = propTypes;

export default ProtectedRoute;
