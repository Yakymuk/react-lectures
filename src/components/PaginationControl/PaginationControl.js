import React from 'react';
import PropTypes from 'prop-types';

import IconButton from 'components/controls/IconButton';
import { PaginationArrow } from 'components/Icons';

import styles from './PaginationControl.scss';

const DEFAULT_EXTRA_PAGE_COUNT = 2;
const GAP_SYMBOL = '...';

const PaginationControl = ({
  page,
  pages,
  extraCount,
  onChange
}) => {
  const items = [];

  if (!pages || pages < 2)
    return null;

  for (let i = 1; i <= pages; i++)
    if (i <= extraCount || i > pages - extraCount || (i >= page - extraCount && i <= page + extraCount))
      items.push(i);
    else if (items[items.length - 1] !== GAP_SYMBOL)
      items.push(GAP_SYMBOL);

  return (
    <div className={styles.mdrPagination}>
      <IconButton
        disabled={page === 1}
        onClick={() => onChange(page - 1)}
        className={styles.mdrPaginationIconBtn}
      >
        <PaginationArrow fillClassName={page > 1 ? styles.mdrPaginationIcon : ''} />
      </IconButton>
      {items.map((p, idx) => (
        <div
          key={p === GAP_SYMBOL ? `_${idx}` : p}
          onClick={() => p !== page && p !== GAP_SYMBOL && onChange(p)}
          className={
            styles.mdrPaginationPage +
            (p === page ? ` ${styles.mdrPaginationPageActive}` : '') +
            (p === GAP_SYMBOL ? ` ${styles.mdrPaginationGap}` : '')
          }
        >
          {p}
        </div>
      ))}
      <IconButton
        disabled={page === pages}
        onClick={() => onChange(page + 1)}
        className={styles.mdrPaginationIconBtn}
      >
        <PaginationArrow
          isRight
          fillClassName={page < pages ? styles.mdrPaginationIcon : ''}
        />
      </IconButton>
    </div>
  );
};

PaginationControl.propTypes = {
  page                    : PropTypes.number.isRequired,
  pages                   : PropTypes.number.isRequired,
  onChange                : PropTypes.func.isRequired,
  extraCount              : PropTypes.number.isRequired
};

PaginationControl.defaultProps = {
  extraCount              : DEFAULT_EXTRA_PAGE_COUNT,
  onChange                : () => {}
};

export default PaginationControl;
