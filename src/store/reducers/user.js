import * as api from '../api/user';
import normalizeFetchReject from 'helpers/promises';

// ------------------------------------
// Constants
// ------------------------------------

export const USER_FETCH_PENDING = 'USER_FETCH_PENDING';
export const USER_FETCH_REJECTED = 'USER_FETCH_REJECTED';
export const USER_FETCH_FULLFILED = 'USER_FETCH_FULLFILED';

export const USER_MENU_SET_STATE = 'USER_MENU_SET_STATE';

// ------------------------------------
// Actions
// ------------------------------------

export const setUserMenuState = (newState = false) => ({
  type    : USER_MENU_SET_STATE,
  payload : newState
});

export function userPending () {
  return {
    type    : USER_FETCH_PENDING
  };
}

export function userFullfiled (info = {}) {
  return {
    type    : USER_FETCH_FULLFILED,
    payload : info
  };
}

export function userRejected (reason = '') {
  return {
    type    : USER_FETCH_REJECTED,
    payload : reason
  };
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------

export const getUserInfo = () => dispatch => {
  dispatch(userPending());

  return api.getCurrentUser()
    .then(info => dispatch(userFullfiled(info)))
    .catch(normalizeFetchReject)
    .catch(msg => dispatch(userRejected()));
};

// ------------------------------------
// Reducer
// ------------------------------------

const initialState = {
  isFetching: true,
  isFetched: false,
  reason: '',
  userRoles: [],
  isMenuVisible: false
};

export default function userReducer (state = initialState, action) {
  switch (action.type) {
    case USER_FETCH_PENDING:
      state = {
        ...state,
        isFetching: true
      };
      break;
    case USER_FETCH_FULLFILED:
      state = {
        ...state,
        isFetching: false,
        isFetched: true,
        ...action.payload
      };
      break;
    case USER_FETCH_REJECTED:
      state = {
        ...state,
        isFetching: false,
        reason: action.payload
      };
      break;
    case USER_MENU_SET_STATE:
      state = {
        ...state,
        isMenuVisible: !!action.payload
      };
      break;
    default:
  }

  return state;
}
