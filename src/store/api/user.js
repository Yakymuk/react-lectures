export function getCurrentUser () {
  return new Promise(resolve => {
    setTimeout(() => resolve({
      userRoles: ['SystemAdmin'],
      name: 'John Doe',
      role: 'System admin'
    }), 1000);
  });
}
