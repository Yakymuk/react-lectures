import { connect } from 'react-redux';

import HelpView from './HelpView';
import { getHelpDocs } from './modules/help';

const mapDispatchToProps = {
  getHelpDocs
};

const mapStateToProps = state => ({
  ...state.help
});

export default connect(mapStateToProps, mapDispatchToProps)(HelpView);
