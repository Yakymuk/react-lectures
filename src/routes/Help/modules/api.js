export function getHelpDocs () {
  return new Promise(resolve => {
    setTimeout(() => resolve([
      {
        name: 'Intro',
        content: 'Bla bla'
      },
      {
        name: 'Getting started',
        content: 'Bla bla'
      },
      {
        name: 'Project information',
        content: 'Bla bla'
      },
      {
        name: 'Team management',
        content: 'Bla bla'
      }
    ]), 1500);
  });
}
