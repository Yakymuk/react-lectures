import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from 'components/Loader';

import styles from './HelpView.scss';

class HelpView extends Component {
  constructor (...args) {
    super(...args);
    this.state = {
      activePage: 0
    };
  }

  static propTypes = {
    isFetched         : PropTypes.bool.isRequired,
    isFetching        : PropTypes.bool.isRequired,
    data              : PropTypes.arrayOf(PropTypes.shape({
      name              : PropTypes.string.isRequired,
      content           : PropTypes.string.isRequired
    })).isRequired,
    getHelpDocs       : PropTypes.func.isRequired
  }

  componentDidMount () {
    this.props.getHelpDocs();
  }

  render () {
    const { isFetching, isFetched, data } = this.props;

    return (
      <div className={styles.beHelp}>
        <h1>Basic Example Help</h1>
        {isFetching &&
        <Loader isBlocking />
        }
        {isFetched &&
          data.map(page => <div key={page.name}>{page.name}</div>)
        }
      </div>
    );
  }
}

export default HelpView;
