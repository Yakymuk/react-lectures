import React, { Component } from 'react';

import ProjectListContainer from './containers/ProjectListContainer';

import styles from './HomeView.scss';

class HomeView extends Component {
  render () {
    return (
      <div className={styles.beHome}>
        <ProjectListContainer />
      </div>
    );
  }
}

export default HomeView;
