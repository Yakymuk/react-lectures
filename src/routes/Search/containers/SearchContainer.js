import { connect } from 'react-redux';

import Search from 'components/Search';
import { searchEntities, searchQueryChange } from 'routes/Search/modules/search';

const mapStateToProps = state => ({
  query             : state.search.query,
  searchResults     : state.search.searchResults
});

const mapDispatchToProps = {
  searchEntities,
  searchQueryChange
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
