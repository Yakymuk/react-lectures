import { connect } from 'react-redux';

import NotFoundView from './NotFoundView';

const mapStateToProps = state => ({
  userRoles: state.user.userRoles
});

export default connect(mapStateToProps)(NotFoundView);
