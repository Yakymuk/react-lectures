import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { HOME_URL } from 'consts';

import styles from './NotFoundView.scss';

const propTypes = {
  userRoles: PropTypes.array.isRequired
};

const NotFoundView = ({ userRoles }) => (
  <div className={styles.beNotFound}>
    <div className={styles.beNotFoundContainer}>
      <div className={styles.beNotFoundImg}>404</div>
      <h1>The page can’t be found or doesn’t exist</h1>
      <Link to={HOME_URL}>Return home</Link>
    </div>
  </div>
);

NotFoundView.propTypes = propTypes;

export default NotFoundView;
