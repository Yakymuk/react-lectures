import React, { Component } from 'react';

import styles from './Sub1View.scss';

class Sub1View extends Component {
  render () {
    return (
      <div className={styles.beAdminSub1}>
        Sub 1 View
      </div>
    );
  }
}

export default Sub1View;
