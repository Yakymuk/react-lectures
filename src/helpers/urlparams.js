function getURLParams (baseURL, queryObject) {
  const result = [];

  Object.keys(queryObject).forEach(key => {
    if (Array.isArray(queryObject[key]))
      queryObject[key].forEach((item, idx) => {
        result.push(`${key}=${encodeURIComponent(queryObject[key][idx])}`);
      });
    else if (typeof queryObject[key] !== 'undefined')
      result.push(`${key}=${encodeURIComponent(queryObject[key])}`);
  });
  return !result.length ? baseURL : `${baseURL}?${result.join('&')}`;
}

export default getURLParams;
