import { STORAGE_KEY } from 'consts';

let storage = {};
let isHaveStorage = false;

function init () {
  // Test support
  try {
    isHaveStorage = 'localStorage' in window && window.localStorage !== null;
  } catch (e) {
    // no throw
  }

  // Restore values
  if (isHaveStorage)
    try {
      storage = JSON.parse(localStorage.getItem(STORAGE_KEY)) || {};
    } catch (e) {
      storage = {};
    }
}

function save () {
  if (isHaveStorage)
    try {
      localStorage.setItem(STORAGE_KEY, JSON.stringify(storage));
    } catch (e) {
      // no throw
    }
}

init();

export function get (key, def) {
  return (key in storage) ? storage[key] : def;
}

export function set (key, value) {
  const oldValue = storage[key];

  storage[key] = value;

  save();

  return oldValue;
}

export default { get, set };
