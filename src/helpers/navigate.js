import { store } from 'main';
import { push } from 'react-router-redux';

function navigate (url = '/') {
  store.dispatch(push(url));
}

export default navigate;
