export const MOCK_USERS = [
  {
    id: 1,
    name: 'Hannah',
    country: 'United Kingdom',
    experience: 2
  },
  {
    id: 2,
    name: 'Samantha',
    country: 'Bulgaria',
    experience: 5
  },
  {
    id: 3,
    name: 'Serhii',
    country: 'Ukraine',
    experience: 3
  },
  {
    id: 4,
    name: 'Andrew',
    country: 'Ukraine',
    experience: 1
  },
  {
    id: 5,
    name: 'Max',
    country: 'Germany',
    experience: 3
  },
  {
    id: 6,
    name: 'Craig',
    country: 'USA',
    experience: 1
  },
  {
    id: 7,
    name: 'Jake',
    country: 'Canada',
    experience: 2
  },
  {
    id: 8,
    name: 'Britanny',
    country: 'Canada',
    experience: 4
  },
  {
    id: 9,
    name: 'Steve',
    country: 'USA',
    experience: 6
  }
];

export const MOCK_PROJECTS = [
  {
    id: 1,
    name: 'Custom project',
    author: 'Hannah',
    startDate: '2018-02-13T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 2,
    name: 'Analytics',
    author: 'Samantha',
    startDate: '2018-02-10T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 3,
    name: 'Social media',
    author: 'Serhii',
    startDate: '2018-02-23T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 4,
    name: 'Learning React',
    author: 'Andrew',
    startDate: '2017-12-13T00:00:00.000Z',
    status: 'Completed'
  },
  {
    id: 5,
    name: 'JavaScript testing',
    author: 'Andrew',
    startDate: '2018-01-03T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 6,
    name: 'Environment problems',
    author: 'Samantha',
    startDate: '2017-11-22T00:00:00.000Z',
    status: 'Completed'
  },
  {
    id: 7,
    name: 'Public relations',
    author: 'Hannah',
    startDate: '2018-02-12T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 8,
    name: 'Social programs',
    author: 'Hannah',
    startDate: '2018-01-31T00:00:00.000Z',
    status: 'Suspended'
  },
  {
    id: 9,
    name: 'Redux state management',
    author: 'Serhii',
    startDate: '2017-10-12T00:00:00.000Z',
    status: 'Completed'
  },
  {
    id: 10,
    name: 'Test environment',
    author: 'Max',
    startDate: '2018-02-03T00:00:00.000Z',
    status: 'Suspended'
  },
  {
    id: 11,
    name: 'Political correctness',
    author: 'Craig',
    startDate: '2017-01-21T00:00:00.000Z',
    status: 'Completed'
  },
  {
    id: 12,
    name: 'Facilitating motivation',
    author: 'Jake',
    startDate: '2018-02-12T00:00:00.000Z',
    status: 'In progress'
  },
  {
    id: 13,
    name: 'Concept of social media',
    author: 'Britanny',
    startDate: '2018-02-20T00:00:00.000Z',
    status: 'Suspended'
  },
  {
    id: 14,
    name: 'Stereotypes at workplace',
    author: 'Steve',
    startDate: '2017-07-11T00:00:00.000Z',
    status: 'Completed'
  },
  {
    id: 15,
    name: 'Global warming',
    author: 'Andrew',
    startDate: '2017-02-01T00:00:00.000Z',
    status: 'Completed'
  }
];
