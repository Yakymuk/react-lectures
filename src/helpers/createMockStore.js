export default function createMockStore (state = {}) {
  return {
    subscribe: () => {},
    dispatch: () => {},
    getState: () => ({ ...state })
  };
}
